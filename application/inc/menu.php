<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
        <li class="nav-item">
          <a class="navbar-brand logo" href="index.php">Accueil</a>
        </li>
      <?php if (isset($_SESSION['profil']) && $_SESSION['profil']==="etudiant"){ ?>
        <li class="nav-item">
            <a class="nav-link" href="index.php?url=etudiant">Interface etudiant</a>
        </li>
      <?php }elseif (isset($_SESSION['profil']) && $_SESSION['profil']==="prof") { ?>
        <li class="nav-item">
            <a class="nav-link" href="index.php?url=prof">Interface prof</a>
        </li>
        <?php }elseif (isset($_SESSION['profil']) && $_SESSION['profil']==="impression") { ?>
        <li class="nav-item">
        <a class="nav-link" href="index.php?url=impression">Impression</a>
        </li>
      <?php }else{ ?>
        <li class="nav-item">
            <a class="nav-link" href="index.php?url=impression">Impression</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="index.php?url=prof">Interface prof</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="index.php?url=etudiant">Interface etudiant</a>
        </li>
        <?php } ?>
        <?php if(isset($_SESSION['profil'])){ ?>
        <li class="nav-item">
            <a class="nav-link" href="index.php?url=logout">Se déconnecter</a>
        </li>
        <?php } ?>
    </ul>
  </div>
</nav>