<?php
if (isset($_SESSION['profil']) && $_SESSION['profil']==="prof"){
    $managerProfesseur = new professeurManager(database::getDB());
    $prof = $managerProfesseur->get($_SESSION['id']);
    $lesClasses = $prof->getLesClasses();
    $classeSelected="";
    $classe = $lesClasses[0];
    //si on a envoyé le formulaire de classe
    if (isset($_POST["classeSelected"])){
      $classeSelected=$_POST["classeSelected"];
      foreach ($lesClasses as $classe) { //on regarde uniquement les classes du prof pour faire la modif
        if($classe->getNum()===$classeSelected){ //si la classe est trouvée, on va la modifier
          $managerClasse = new classeManager(database::getDB());
          $classe->setDetailSection($_POST["detailSectionClasse"]);
          $classe->setMissionsType($_POST["missionsTypeClasse"]);
          $classe->setDatesStage($_POST["datesStageClasse"]);
          $managerClasse->save($classe);
          $message="Modification effectuée";
        }
      }
    } else if (isset($_POST["idEtudiant"])){ //si on a envoyé le formulaire étudiant
      $managerEtudiant = new etudiantManager(database::getDB());
      $managerClasse = new classeManager(database::getDB());
      $classe = $managerClasse->get($_POST["classeSelectedEtudiant"]);
      $classeSelected = $_POST["classeSelectedEtudiant"];

      if ($_POST["idEtudiant"]!="" && $_POST["suppressionEtudiant"]=="false"){ //on fait alors une modif
        //je ne vérifie pas ici que l'étudiant est bien un étudiant d'une classe du prof connecté
        $etudiant = $managerEtudiant->get($_POST["idEtudiant"]);
        $etudiant->setNom($_POST["nomEtudiant"]);
        $etudiant->setPrenom($_POST["prenomEtudiant"]);
        $etudiant->setMail($_POST["mailEtudiant"]);
        $etudiant->setTelephone($_POST["telEtudiant"]);
        $etudiant->setDescription($_POST["descriptionEtudiant"]);
        $managerEtudiant->save($etudiant);
        $message="Modification effectuée";
      }else if ($_POST["idEtudiant"]!="" && $_POST["suppressionEtudiant"]=="true"){ //on fait alors une suppression
        $etudiant = $managerEtudiant->get($_POST["idEtudiant"]);
        $managerEtudiant->delete($etudiant);
        $message="Suppression effectuée";
      }else{ //on fait un ajout d'étudiant
        
        $etudiant = new etudiant($_POST["nomEtudiant"], $_POST["prenomEtudiant"], $_POST["mailEtudiant"], $_POST["descriptionEtudiant"], $_POST["mailEtudiant"], $_POST["prenomEtudiant"], $classe, $_POST["telEtudiant"]);
        $managerEtudiant->save($etudiant);
        $message="Ajout effectué";
      }
    }


}else{
  // pas connecté
  header("Location: index.php?url=login&profil=prof");

}

?>

<article>
    <?php
    if (isset($message)) {
      echo "<p style='color:green;'>".$message."</p><br>";
    }
    ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>
            
            <form method="POST" action="index.php?url=prof" class="form-horizontal col-xs-12 col-sm-12 col-md-9 col-lg-9" onreset="enModifDescriptionClasse()">
              <?php //récupération des classes du prof
              foreach ($lesClasses as $classe) { //bricole permettant de récupérer les données en javascript dans le DOM sans recharger...
                echo '<input type="hidden" id="dsc'.$classe->getNum().'" value="'.$classe->getDetailSection().'">';
                echo '<input type="hidden" id="mt'.$classe->getNum().'" value="'.$classe->getMissionsType().'">';
                echo '<input type="hidden" id="ds'.$classe->getNum().'" value="'.$classe->getDatesStage().'">';
              }
              ?>
              <fieldset>

                <img class="avatar" src="../img/avatar.png">
                <h1>Bienvenue <?= $prof->getPrenom(). " " .$prof->getNom() ?></h1>
                <hr>



                <div class="form-group">
                  <label for="select" class="col-lg-2 control-label">Classes</label>
                  <div class="col-lg-10">
                    <select class="form-control" id="selectClasse" onchange="choisirClasse()">
                      <?php
                      $rowSelect=0; //pour savoir quelle ligne du select est sélectionnée par défaut
                      $i=0;
                      foreach ($lesClasses as $classe) {
                        if($classeSelected===$classe->getNum()){
                          echo "<option selected value=".$classe->getNum().">".$classe->getNom()."</option>";
                          $rowSelect=$i;                          
                        }else{
                          echo "<option value=".$classe->getNum().">".$classe->getNom()."</option>";
                        }
                        $i++;
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <input type="hidden" id="classeSelected" name="classeSelected" value="<?= $lesClasses[$rowSelect]->getNum()?>">
              
                  <div class="form-group">
                      <label for="detailSectionClasse" class="col-lg-2 control-label">Description :</label>
                      <div class="col-lg-10">
                          <textarea required id="detailSectionClasse" name="detailSectionClasse" class="form-control" rows="1" placeholder="Description" onkeyup="disModifDescriptionClasse()"><?= $lesClasses[$rowSelect]->getDetailSection() ?></textarea>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="missionsTypeClasse" class="col-lg-2 control-label">Missions type :</label>
                      <div class="col-lg-10">
                          <textarea required id="missionsTypeClasse" name="missionsTypeClasse" class="form-control" rows="5" placeholder="Les missions que sont à même de réaliser vos étudiants" onkeyup="disModifDescriptionClasse()"><?= $lesClasses[$rowSelect]->getMissionsType() ?></textarea>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="datesStageClasse" class="col-lg-2 control-label">Dates de stage :</label>
                      <div class="col-lg-10">
                          <textarea required id="datesStageClasse" name="datesStageClasse" class="form-control" rows="2" placeholder="Les dates du ou des stages que doivent faire les étudiants (ex: du 25/02/2019 au 25/04/2019)" onkeyup="disModifDescriptionClasse()"><?= $lesClasses[$rowSelect]->getDatesStage() ?></textarea>
                          <span class="help-block">Tous les champs sont obligatoires</span>
                      </div>
                  </div>
                  
                  <div class="form-group">
                  <h2>Etudiants de la classe:</h2>
                  </div>

                  <div class="form-group" id="zoneEtudiants" name="zoneEtudiants">
                      
                  </div>




                <div class="form-group">
                  <div class="col-lg-10 col-lg-offset-2">
                    <button type="reset" class="btn btn-default">Annuler</button>
                    <button type="submit" name="submit" class="btn btn-primary">Enregistrer</button>
                  </div>
                </div>

            </fieldset>
            </form>

            <div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>

              <form id="formEtudiant" name="formEtudiant" method="POST" action="index.php?url=prof" class="form-horizontal col-xs-12 col-sm-12 col-md-9 col-lg-9">             
                <fieldset>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Etudiant :</label>
                        <div class="col-lg-10">
                            <input type="hidden" id="suppressionEtudiant" name="suppressionEtudiant" value="false" class="form-control">
                            <input type="hidden" id="idEtudiant" name="idEtudiant" class="form-control">
                            <input type="text" id="nomEtudiant" name="nomEtudiant" class="form-control" placeholder="Nom de l'étudiant" required>
                            <input type="text" id="prenomEtudiant" name="prenomEtudiant" class="form-control" placeholder="Prénom de l'étudiant" required>
                            <input type="text" id="mailEtudiant" name="mailEtudiant" class="form-control" placeholder="Mail de l'étudiant" required>
                            <input type="text" id="telEtudiant" name="telEtudiant" class="form-control" placeholder="Téléphone de l'étudiant">
                            <textarea rows="8" id="descriptionEtudiant" name="descriptionEtudiant" class="form-control" placeholder="Description de l'étudiant"></textarea>
                            <input type="hidden" id="classeSelectedEtudiant" name="classeSelectedEtudiant" value="<?= $lesClasses[$rowSelect]->getNum()?>">

                        </div>
                    </div>
                  


                  <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                      <button type="reset" class="btn btn-default" id="btReset" onclick="viderHidden()">Annuler</button>
                      <button type="submit" name="btSubmit" id="btSubmit" class="btn btn-primary">Ajouter</button>
                      <button type="button" class="btn btn-danger" id="btDelete" onclick="supprimerEtudiant()">Supprimer</button>
                    </div>
                  </div>

                </fieldset>
              </form>

        </div>
    </div>
</article>

<script src="./js/metier/etudiant.js" ></script>
<script>
chargerEtudiants()
</script>