<?php
if (isset($_SESSION['profil']) && $_SESSION['profil']==="impression"){

//require($base_url . 'functions/functions.php');
//recupération des data 
$managerListes = new listeManager(database::getDB());

//si ont vient d'imprimer une liste, on change son statut
if (isset($_GET['impression']) && $_GET['impression'] != "") {
    $liste = $managerListes->get($_GET['impression']);
    if ($liste) { // si la liste est trouvée
        $liste->setStatut(1);
        $managerListes->save($liste);
    }
}

$listeAImprimer = $managerListes->getList("WHERE statutListe=0 ORDER by idListe DESC;");
$toutesListes = $managerListes->getList("ORDER by idListe DESC;");
$listeImprimees = $managerListes->getList("WHERE statutListe=1 ORDER by idListe DESC;");


}else{
    // pas connecté
    header("Location: index.php?url=login&profil=impression");
}
?>
<article>
    <div class="container print">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">A imprimer</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Imprimées</a>
            </li>
            
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"> <!--La zone des listes à imprimer -->
                <h2>A imprimer</h2>
                <button type="button" class="btn btn-outline-success btn-block" onclick="location.reload();">Actualiser la liste</button><br><br>

                <div class="list-group">
                    <?php
                    foreach ($listeAImprimer as $liste) {
                        echo '<a onclick="window.location.replace(\'index.php?url=impression&impression='.$liste->getNum().'\')" target="blank" href="pages/afficherListes.php?liste='.$liste->getNum().'" class="list-group-item">'.$liste->getOrganisation().' </a>';
                    }
                    ?>
                </div>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab"><!--La zone des listes imprimées -->
                <h2>Imprimées</h2>
                <div class="list-group">
                    <?php
                    foreach ($listeImprimees as $liste) {
                        echo '<a target="blank" href="pages/afficherListes.php?liste='.$liste->getNum().'" class="list-group-item">'.$liste->getOrganisation().' </a>';
                    }
                    ?>
                </div>
            </div>
            
        </div>
    </div>

</article>
