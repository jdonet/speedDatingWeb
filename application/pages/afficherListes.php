<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
  function __autoload($class_name) {
    require( '../../classes/' . $class_name . '.class.php');
  }
    

if (isset($_SESSION['profil']) && $_SESSION['profil']==="impression" && isset($_GET['liste']) && $_GET['liste']!=""){

//require($base_url . 'functions/functions.php');
//recupération des data 
$managerListes = new listeManager(database::getDB());

$classes = $managerListes->getClasses($_GET["liste"]);
}else{
    // pas connecté
    header("Location: ../index.php?url=login&profil=impression");
}
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<style>
pre {
  word-wrap: break-word;      /* IE 5.5-7 */
  white-space: -moz-pre-wrap; /* Firefox 1.0-2.0 */
  white-space: pre-wrap;      /* current browsers */
}
</style>
<article>

    <div class="table-responsive-sm">
    <table class="table">

        <?php 

                foreach ($classes as $classe) {
                    echo '<thead class="thead"><tr><th colspan="4"><b>Classe :</b> '. $classe->getNom(). ' <b> Dates de stage: </b>'.$classe->getDatesStage().'.</th></tr></thead>';
                    echo '<tr><td colspan="4"><b>Missions types : </b><pre>'. $classe->getMissionsType(); '.</pre></td></tr>';
                    
                    //recup des etudiants de la classe
                    $managerEtudiant = new etudiantManager(database::getDB());
                    $listeEtudiants = $managerEtudiant->getList("WHERE classeEtudiant = ". $classe->getNum());

                ?>
                <tr>
                    <td scope="col">Classe</td>
                    <td scope="col">Nom</td>
                    <td scope="col">Prenom</td>
                    <td scope="col">Mail</td>
                </tr>
                <?php 
                $classes=[];
                    foreach ($listeEtudiants as $etudiant) {
                        //ajout des classes
                        if(!in_array($etudiant->getClasse(), $classes)) {
                            $classes[] = $etudiant->getClasse();
                        }
                        ?>
                        <tr>
                            <td><?php echo $etudiant->getClasse()->getNom(); ?></td>
                            <td><?php echo $etudiant->getNom(); ?></td>
                            <td><?php echo $etudiant->getPrenom(); ?></td>
                            <td><?php echo $etudiant->getMail(); ?></td>
                        </tr> 
                        <tr>
                            <td colspan="4"><pre><?php echo $etudiant->getDescription(); ?></pre></td>
                        </tr>

                        <?php
                }
                ?>
            <?php 

        }
        ?>
        </table>
        </div>
</article>
