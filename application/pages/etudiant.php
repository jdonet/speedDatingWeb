<?php
if (isset($_SESSION['profil']) && $_SESSION['profil']==="etudiant"){
    $managerEtudiant = new etudiantManager(database::getDB());
    $etudiant = $managerEtudiant->get($_SESSION['id']);

    //si on a envoyé le formulaire
    if (isset($_POST["mail"])){
        $etudiant->setNom($_POST["nom"]);
        $etudiant->setPrenom($_POST["prenom"]);
        $etudiant->setMail($_POST["mail"]);
        $etudiant->setTelephone($_POST["tel"]);
        $etudiant->setDescription($_POST["description"]);
        $managerEtudiant->save($etudiant);
    }


}else{ // pas connecté
	header("Location: index.php?url=login&profil=etudiant");
}
?>

<article>
    <div class="container">
        <div class="row">
            <div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>

            <form method="POST" action="index.php?url=etudiant" class="form-horizontal col-xs-12 col-sm-12 col-md-9 col-lg-9">
              <fieldset>

                <img class="avatar" src="../img/avatar.png">
                <h1>Bienvenue <?= $etudiant->getPrenom(). " " .$etudiant->getNom() ?></h1>
                <hr>

                <div class="form-group">
                    <label for ="nom" class="col-lg-2 control-label">Nom :</label>
                    <div class="col-lg-10">
                        <input required type="text" class="form-control" name="nom" value="<?= $etudiant->getNom()?>" placeholder="Nom" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label for ="prenom" class="col-lg-2 control-label">Prenom:</label>
                    <div class="col-lg-10">
                        <input required type="text" class="form-control" name="prenom" value="<?= $etudiant->getPrenom()?>" placeholder="Prenom" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label for ="mail" class="col-lg-2 control-label">Mail :</label>
                    <div class="col-lg-10">
                        <input required type="email" class="form-control" name="mail" value="<?= $etudiant->getMail()?>" placeholder="Email@email.com" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label for ="tel" class="col-lg-2 control-label">Numero Tel :</label>
                    <div class="col-lg-10">
                        <input required type="text" class="form-control" name="tel"  value="<?= $etudiant->getTelephone()?>" placeholder="+689 87 987987" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="col-lg-2 control-label">Description :</label>
                    <div class="col-lg-10">
                        <textarea required name="description" class="form-control" rows="8" placeholder="Décrire votre profil pour donner envie à un recruteur de vous rencontrer" ><?= $etudiant->getDescription()?></textarea>
                        <span class="help-block">Tous les champs sont obligatoires</span>
                    </div>
                </div>

                <div class="form-group">
                    <button type="reset" class="btn btn-default">Annuler</button>
                    <button type="submit" name="submit" class="btn btn-primary">Enregistrer</button>
                </div>

            </fieldset>
            </form>
            
            <div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>
        </div>
    </div>
</article>