<?php
//utilisation opérateur ternaire
$profil = (empty($_GET['profil'])) ? 'impression' : $_GET['profil'];

if (!empty($_POST)) {
	switch ($profil) {
		case 'prof':
			//professeur
			$managerProfesseur = new professeurManager(database::getDB());

			$pseudoconnexion = htmlspecialchars($_POST['username']);
			$passwordconnexion = htmlspecialchars($_POST['password']);

			if (!empty($pseudoconnexion) AND !empty($passwordconnexion)) {
				$listeProf = $managerProfesseur->getList("WHERE loginProf = '".$pseudoconnexion."'");

				foreach ($listeProf as $prof) {
					if ($prof->getPass()===$passwordconnexion) {

						$_SESSION['id']=$prof->getId();
						$_SESSION['pseudo']=$prof->getLogin();
						$_SESSION['profil']="prof";
						header("Location: index.php?url=prof");
					}
					else{
						$erreur="Mauvais pseudo ou mot de passe";
					}
				}
			}
			else{
				$erreur="Tout les champs doivent etre complétés";
			}
		break;

		case 'etudiant':
			//etudiant
			$managerEtudiant = new etudiantManager(database::getDB());
			$pseudoconnexion = htmlspecialchars($_POST['username']);
			$passwordconnexion = htmlspecialchars($_POST['password']);

			if (!empty($pseudoconnexion) AND !empty($passwordconnexion)) {
				$listeEtu = $managerEtudiant->getList("WHERE loginEtudiant = '".$pseudoconnexion."'");
				if(empty($listeEtu)){
					$erreur="Mauvais pseudo";
				}else{
					foreach ($listeEtu as $etu) {
						if ($etu->getPass()===$passwordconnexion) {

							$_SESSION['id']=$etu->getId();
							$_SESSION['pseudo']=$etu->getLogin();
							$_SESSION['profil']="etudiant";
							header("Location: index.php?url=etudiant");
						}
						else{
							$erreur="Mauvais mot de passe";
						}
					}
				}

			}
			else{
				$erreur="Tout les champs doivent etre complétés";
			}
		break;

		case 'impression':
			//print
			$pseudoconnexion = htmlspecialchars($_POST['username']);
			$passwordconnexion = htmlspecialchars($_POST['password']);

			if (!empty($pseudoconnexion) AND !empty($passwordconnexion)) {
				if ($pseudoconnexion=="print" && $passwordconnexion=="print") {
					$_SESSION['profil']="impression";
					header("Location: index.php?url=impression#home");
				}else{
						$erreur="Mauvais pseudo ou mot de passe";
				}
			}
			else{
				$erreur="Tout les champs doivent etre complétés";
			}
		break;
	}
	}

?>

<script type="text/javascript" src="<?php echo $base_url . "js/script.js"; ?>"></script>

<article>

<div class="container">
	<div class="row">
		<div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>
		<form action="" method="POST" class="col-xs-12 col-sm-12 col-md-9 col-lg-9">

		<img class="avatar" src="../img/avatar.png">

		<h1>Se connecter / <?= $profil?></h1>
		<hr>

		<div class="form-group">
			<label for="username">Pseudo</label>
			<input type="text" name="username" class="form-control" placeholder="Pseudo">
		</div>
		<br>

		<div class="form-group">
			<label for="password">Mot de passe</label>
			<input type="password" name="password" class="form-control" placeholder="Mot de passe">
		</div>
		<br>

		<?php
		if (isset($erreur)) {
			echo "<p style='color:red;'>".$erreur."</p><br>";
		}
		?>

		<button type="submit" class="btn btn-primary">Se connecter</button>

	</form>
	<div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>

	</div>

</div>
</article>
