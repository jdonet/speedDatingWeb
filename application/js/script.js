var tousLesEtudiants=[];
var etudiantsFiltres=[];
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function imprimer(liste){
    
    var url="index.php?url=impression&impression="+liste;
    window.location = url;
    window.print();
}

function choisirClasse(){
    var select = document.getElementById("selectClasse");
    document.getElementById("classeSelected").value = select.value; // maj classe
    document.getElementById("classeSelectedEtudiant").value = select.value; // maj classe etudiant

    //maj de la description
    document.getElementById("detailSectionClasse").value=document.getElementById("dsc"+select.value).value;
    document.getElementById("missionsTypeClasse").value=document.getElementById("mt"+select.value).value;
    document.getElementById("datesStageClasse").value=document.getElementById("ds"+select.value).value;
    
    //maj de la liste des étudiants
    filterEtudiant(document.getElementById("classeSelected").value)
    majZoneEtudiant()
}

function disModifDescriptionClasse(){
    document.getElementById("selectClasse").disabled="disabled";
}
function enModifDescriptionClasse(){
    document.getElementById("selectClasse").removeAttribute("disabled");
}

function chargerEtudiants(){

//on masque le bouton de suppresion
document.getElementById('btDelete').style.display="none";

    var httpRequest = false;
    httpRequest = new XMLHttpRequest();
    httpRequest.overrideMimeType("application/json");

    if (!httpRequest) {
        alert('Abandon :( Impossible de créer une instance XMLHTTP');
        return false;
    }
    httpRequest.onreadystatechange = function() { 
        if (httpRequest.readyState == XMLHttpRequest.DONE) {
            if (httpRequest.status == 200) {
                var etudiants = JSON.parse(httpRequest.responseText);
                //console.log(JSON.parse(etudiants[i].id))
                //alert(etudiants)
                for (var i=0;i<etudiants.length;i++){
                    //création des instances d'étudiants
                    var etu = new Etudiant(etudiants[i].id,etudiants[i].nom,etudiants[i].prenom,etudiants[i].mail,etudiants[i].description,etudiants[i].classe.num,etudiants[i].telephone);
                    tousLesEtudiants[i]= etu
                }
                //on lance enfin le filtre et l'affichage
                choisirClasse()

            } else {
                alert('Un problème est survenu avec la requête.');
            }
        }
    };
    httpRequest.open('GET', "../ws/ws.php?action=getEtudiants", true);
    httpRequest.send(null);
}

function majZoneEtudiant(){
    var zone = document.getElementById("zoneEtudiants")
    zone.innerHTML = ''
    for (var i=0;i<etudiantsFiltres.length;i++){
        zone.innerHTML += ' <a onclick="modifierEtudiant('+i+')" class="list-group-item list-group-item-action">'+etudiantsFiltres[i].sePresenter()+'</a>'
    }
}


function filterEtudiant(classe){
    //on vide la liste des étudiants filtrés
    etudiantsFiltres=[]
    var j=0
   
    for (var i=0;i<tousLesEtudiants.length;i++){
        if (tousLesEtudiants[i].classe == classe){
            etudiantsFiltres[j] = tousLesEtudiants[i];
            j++;   
        }
    }
}

function modifierEtudiant(num){
    document.getElementById("idEtudiant").value = etudiantsFiltres[num].id
    document.getElementById("nomEtudiant").value = etudiantsFiltres[num].nom
    document.getElementById("prenomEtudiant").value = etudiantsFiltres[num].prenom
    document.getElementById("mailEtudiant").value = etudiantsFiltres[num].mail
    document.getElementById("descriptionEtudiant").value = etudiantsFiltres[num].description
    document.getElementById("telEtudiant").value = etudiantsFiltres[num].telephone
    document.getElementById('btSubmit').innerHTML='Modifier';
    document.getElementById('btReset').innerHTML='Annuler / Ajouter';
    document.getElementById('btDelete').style.display="inline";
}


function viderHidden(){
    document.getElementById('idEtudiant').value='';
    document.getElementById('btSubmit').innerHTML='Ajouter';
    document.getElementById('btReset').innerHTML='Annuler';
    document.getElementById('btDelete').style.display="none";
}

function supprimerEtudiant(){
    document.getElementById('suppressionEtudiant').value='true';
    document.forms["formEtudiant"].submit();

}