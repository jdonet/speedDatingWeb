function Etudiant(id,nom,prenom,mail,description,classe,telephone) {
    this.id = id;
    this.nom = nom;
    this.prenom = prenom;
    this.mail = mail;
    this.description = description;
    this.classe = classe;
    this.telephone = telephone;
  }
  
  Etudiant.prototype.sePresenter = function() {
    return this.nom + " " +this.prenom;
  };
  