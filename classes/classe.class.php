<?php

/**
 * Classe représentant les classes.
 * 
 * Les classes sont définies par
 *      <br>- un numéro unique
 *      <br>- un nom
 *      <br>- une section
 *      <br>- un detailSection
 * 
 * @author Julien
 */
class classe implements JsonSerializable {

    private $num;
    private $nom;
    private $section;
    private $detailSection;
    private $missions;
    private $datesStage;

    /**
     * Instancie un objet classe
     * @param type $nom : Le nom de la classe
     * @param type $section : La section de la classe en initiales
     * @param type $detailSection : La section de la classe en toutes lettres
     * @param type $missions : Les missions types de la classe en toutes lettres
     * @param type $datesStage : Les dates de stage de la classe en toutes lettres
     */
    public function __construct($nom, $section, $detailSection,$missions,$datesStage) {
        $this->nom = $nom;
        $this->section = $section;
        $this->detailSection = $detailSection;
        $this->missions = $missions;
        $this->datesStage = $datesStage;
    }

    // Mutateurs chargés de modifier les attributs
    public function setNum($n) {
        $this->num = $n;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setSection($s) {
        $this->section = $s;
    }

    public function setDetailSection($dc) {
        $this->detailSection = $dc;
    }
    public function setMissionsType($m) {
        $this->missions = $m;
    }
    public function setDatesStage($d) {
        $this->datesStage = $d;
    }

    // Accesseurs chargés d'exposer les attributs
    public function getNum() {
        return $this->num;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getSection() {
        return $this->section;
    }

    public function getDetailSection() {
        return $this->detailSection;
    }

    public function getMissionsType() {
        return $this->missions;
    }

    public function getDatesStage() {
        return $this->datesStage;
    }
    /**
     * Spécifie les données qui doivent être linéarisées en JSON.
     * 
     * Linéarise l'objet en une valeur qui peut être linéarisé nativement par la fonction json_encode().
     * 
     * @return mixed Retourne les données qui peuvent être linéarisées par la fonction json_encode()
     */
    public function jsonSerialize() {
        return [
            'num' => $this->num,
            'nom' => $this->nom,
            'section' => $this->section,
            'detailSection' => $this->detailSection,
            'missions' => $this->missions,
            'datesStage' => $this->datesStage
        ];
    }

}
