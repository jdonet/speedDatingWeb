<?php
require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les listes.
 * 
 * @author Julien
 */
class listeManager {
    
    private $db;
    
    /**
     * Instancie un objet listeManager.
     * 
     * Permet d'instanicer un objet listeManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */    
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    
    /**
     * Enregistre ou Modifie la liste dans la base.
     * 
     * Pour enregistrer la liste passée en paramètre dans la base de données :
     *      <br>UPDATE si la liste est déjà existante;
     *      <br>INSERT sinon (si num non trouvé ou non spécifié).
     * 
     * @param liste Liste à enregister ou mettre à jour.
     * 
     * @return int Retourne le numéro de la liste ajoutée ou mise à jour.
     */
    public function save(liste $liste)
    {        
        $nbRows = 0;

        // la liste que nous essayons de sauvegarder existe-t-elle dans la  base de données ?
        if ($liste->getNum()!=''){
            $query = "select count(*) as nb from `liste` where `idListe`=?";
            $traitement = $this->db->prepare($query);
            $param1=$liste->getNum();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }
        
        // Si la liste que nous sauvegardons existe dans la bd : UPDATE
        if ($nbRows > 0)
        {
            //Supression des tuples existant dans contenuListe
            $query = "DELETE FROM contenuListe WHERE refListe=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $liste->getNum();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            
            //On récupère les tableaux d'étudiants de la liste
            $tabEtud = $liste->getEtudiants();
            
            //parcours du tableau d'étudiants et insertion des tuples dans contenuListe
            foreach ($tabEtud as $etud) {
                $query = "insert into `contenuListe` (`refListe`, `refEtudiant`) values (?,?);";
                $traitement = $this->db->prepare($query);
                $param1=$liste->getNum();
                $traitement->bindparam(1,$param1);
                $param2=$etud->getId();
                $traitement->bindparam(2,$param2);
                $traitement->execute();
            }

            //Mis à jour de la table liste
            $query = "update `liste` set `nomOrganisationListe`=?, `statutListe`=? where `idListe`=?;";
            $traitement = $this->db->prepare($query);
            $param1=$liste->getOrganisation();
            $traitement->bindparam(1,$param1);
            $param2=$liste->getStatut();
            $traitement->bindparam(2,$param2);
            $param3=$liste->getNum();
            $traitement->bindparam(3,$param3);
            $traitement->execute();
        }
        // sinon nouvelle liste : INSERT
        else
        {
            $query = "insert into `liste` (`nomOrganisationListe`, `statutListe`) values (?,?);";
            $traitement = $this->db->prepare($query);
            $param1=$liste->getOrganisation();
            $traitement->bindparam(1,$param1);
            $param2=$liste->getStatut();
            $traitement->bindparam(2,$param2);
            $traitement->execute();
            
            $liste->setNum($this->db->lastInsertId());
            
            //On récupère les tableaux d'étudiants de la liste
            $tabEtud = $liste->getEtudiants();

            //parcours du tableau d'étudiants et insertion des tuples dans contenuListe
            foreach ($tabEtud as $etud) {
                $query = "insert into `contenuListe` (`refListe`, `refEtudiant`) values (?,?);";
                $traitement = $this->db->prepare($query);
                $param1=$liste->getNum();
                $traitement->bindparam(1,$param1);
                $param2=$etud->getId();
                $traitement->bindparam(2,$param2);
                $traitement->execute();
            }            
        }
        
        return $liste->getNum();
    }

    /**
     * Supprime la liste de la base.
     * 
     * @param liste Objet liste devant être supprimé.
     * @return boolean Retourne true si la suppression est un succès, false sinon.
     */
    public function delete(liste $liste)
    {
        $nbRows = 0;

        // la liste que nous essayons de supprimer existe-t-elle dans la  bd ?
        if ($liste->getNum()!=''){                    
            $query = "select count(*) as nb from `liste` where `idListe`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $liste->getNum();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // SI la liste que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM liste (suppression en cascade pour contenuListe)
        //          et retourne TRUE
        if ($nbRows > 0)
        {
            // DELETE FROM liste
            $query = "DELETE FROM liste WHERE idListe=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $liste->getNum();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            
            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }
    
    /**
     * Sélectionne un(des) liste(s) dans la base.
     * 
     * Méthode générique de SELECT qui renvoie un tableau de liste correspondant aux critères de sélection spécifiés.
     * Si aucun paramètre n'est précisé, la valeur par défaut du paramètre 'WHERE 1' permet d'obtenir toutes les listes.
     * 
     * @param string Chaîne de caractère devant être une restriction SQL valide.
     * @return array Renvoie un tableau d'objet(s) liste.
     */
    public function getList($restriction='WHERE 1')
    {
        $query = "select * from `liste` ".$restriction.";";
        $listeList = Array();
        //execution de la requete
        try
        {
            $result = $this->db->Query($query);
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //Parcours du jeu d'enregistrement
        //Chaque ligne comporte les colonnes idListe, organisationListe, statutListe 
        while ($row = $result->fetch())
        {
            //appel du constructeur paramétré avec le client correspondant à l'id de la base de données
            $managerEtu = new etudiantManager(database::getDB());
            $liste = new liste($row['nomOrganisationListe']);

            //positionnement des valeurs dans les attributs
            $liste->setStatut($row['statutListe']);
            $liste->setNum($row['idListe']);
            //parcours de la table contenuListe pour trouver le contenu de la liste
            $queryContenu = "select * from `contenuListe` where refListe='".$row['idListe']."'";
            try
            {
                $resultContenu = $this->db->Query($queryContenu);
            }
            catch(PDOException $e)
            {
                die ("Erreur : ".$e->getMessage());
            }

            //Parcours du jeu d'enregistrement de contenuListe
            while ($rowContenu = $resultContenu->fetch())
            {
                //On instancie un objet etudiant
                $etudiant = $managerEtu->get($rowContenu['refEtudiant']);
                $liste->addEtudiant($etudiant);
            }

            //ajout de l'objet liste bien rempli à la fin du tableau
            $listeList[] = $liste;
        }
        //retourne le tableau d'objets 'liste'
        return $listeList;   
    }
    
    /**
     * Sélectionne une liste dans la base.
     * 
     * Méthode de SELECT qui renvoie la liste dont le numéro est spécifié en paramètre.
     * 
     * @param int Numéro de la liste recherchée
     * @return produit|boolean Renvoie la liste recherchée ou FALSE si elle n'a pas été trouvée
     */
    public function get($num)
    {
        $query = "select * from `liste` WHERE `idListe`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$num);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }
        //On récupère la première et seule ligne du jeu d'enregistrement	
        if ($row = $traitement->fetch()) {        
             //appel du constructeur paramétré avec le client correspondant à l'id de la base de données
            $managerEtu = new etudiantManager(database::getDB());
            $liste = new liste($row['nomOrganisationListe']);

            //positionnement des valeurs dans les attributs
            $liste->setStatut($row['statutListe']);
            $liste->setNum($row['idListe']);

            //parcours de la table contenuListe pour trouver le contenu de la liste
            $queryContenu = "select * from `contenuListe` where refListe='".$row['idListe']."'";
            try
            {
                $resultContenu = $this->db->Query($queryContenu);
            }
            catch(PDOException $e)
            {
                die ("Erreur : ".$e->getMessage());
            }

            //Parcours du jeu d'enregistrement de contenuListe
            while ($rowContenu = $resultContenu->fetch())
            {
                //On instancie un objet etudiant
                $etudiant = $managerEtu->get($rowContenu['refEtudiant']);
                $liste->addEtudiant($etudiant);
            }
            //retourne l'objet 'liste' correpsondant
            return $liste;
        }
        else {
            return false;
        }
    }
	/**
     * Affiche les classes de la liste dont l id est passe en parametre.
     * 
     * 
     * @param int Numéro de la liste recherchée
     * @return classes|[] Renvoie la liste des classes ou tableau vide si elle n'a pas été trouvée
     */
    public function getClasses($num)
    {
        $query = "select distinct classeEtudiant from `contenuListe`,`etudiant` where idEtudiant=refEtudiant AND refListe=?";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$num);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }
        $liste=[];
        //On récupère la première et seule ligne du jeu d'enregistrement	
        while ($row = $traitement->fetch()) {        
             //appel du constructeur paramétré avec le client correspondant à l'id de la base de données
            $managerClasse = new classeManager(database::getDB());
            //On instancie un objet etudiant
            $classe = $managerClasse->get($row['classeEtudiant']);
            $liste[]= $classe;
            
        }
        return $liste;
        
    }
	

}