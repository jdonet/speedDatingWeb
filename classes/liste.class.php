<?php
/**
 * Classe représentant les listes.
 * 
 * Les listes sont définis par
 *      <br>- un numéro unique
 *      <br>- un nom d'organisation
 *      <br>- un statut (0 pour à imprimer, 1 pour imprimé)
 *      <br>- un tableau d'objet etudiant
 * 
 * @author Julien
 */
class liste implements JsonSerializable {
    private $num;
    private $organisation;
    private $statut;
    private $etudiants=array(); //un tableau d'objet produit

    /**
     * Instancie un objet liste.
     * 
     * @param client Client qui passe la liste.
     */
    public function __construct($orga){
        $this->organisation = $orga;
        $this->statut = 0;
    }
    
    // Mutateurs chargés de modifier les attributs
    public function setNum($n){$this->num = $n;}    
    public function setOrganisation($o){$this->organisation = $o;}
    public function setStatut($s){$this->statut = $s;}
    
    // Accesseurs chargés d'exposer les attributs
    public function getNum(){return $this->num;}
    public function getOrganisation(){return $this->organisation;}
    public function getStatut(){return $this->statut;}
    public function getEtudiants(){return $this->etudiants;}
    
    /**
     * Permet d'ajouter un étudiant à la liste.
     * 
     * @param etudiant Etudiant à ajouter à la liste.
     */
    public function addEtudiant(etudiant $etu){
        /* Ajout d’1 étudiant à la fin au tableau etudiants[] */        
        array_push($this->etudiants,$etu);
    }
    
    
    
    /**
     * Permet de retirer un étudiant de la liste.
     * 
     * @param etudiant Etudiant à retirer de la liste.
     */
    public function removeProduit(etudiant $etu){
        $index = array_search($etu, $this->etudiants);
        unset($this->etudiants[$index]); 
    }
    
    
    /**
     * Spécifie les données qui doivent être linéarisées en JSON.
     * 
     * Linéarise l'objet en une valeur qui peut être linéarisé nativement par la fonction json_encode().
     * 
     * @return mixed Retourne les données qui peuvent être linéarisées par la fonction json_encode()
     */
    public function jsonSerialize() {
        return [
            'num' => $this->num,
            'organisation' => $this->organisation,
            'statut' => $this->statut,
            'etudiants' => $this->etudiants
        ];
    }
}