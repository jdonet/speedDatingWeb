<?php

require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les classes.
 * 
 * @author Julien
 */
class classeManager {

    private $db;

    /**
     * Instancie un objet classeManager.
     * 
     * Permet d'instanicer un objet classeManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database) {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db = $database;
    }

    /**
     * Enregistre ou Modifie la classe dans la base.
     * 
     * Pour enregistrer la classe passée en paramètre dans la base de données :
     *      <br>UPDATE si la classe est déjà existante;
     *      <br>INSERT sinon (si num non trouvé ou non spécifié).
     * 
     * @param classe Classe à enregister ou mettre à jour.
     * 
     * @return int Retourne le numéro de la commande ajoutée ou mise à jour.
     */
    public function save(classe $classe) {
        $nbRows = 0;

        // la classe que nous essayons de sauvegarder existe-t-elle dans la  base de données ?
        if ($classe->getNum() != '') {
            $query = "select count(*) as nb from `classe` where `idClasse`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $classe->getNum();
            $traitement->bindparam(1, $param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows = $ligne[0];
        }

        // Si la classe que nous sauvegardons existe dans la bd : UPDATE
        if ($nbRows > 0) {



            //Mise à jour de la table classe
            $query = "update `classe` set `nomClasse`=?, `sectionClasse`=?, `detailSectionClasse`=?, `missionsTypeClasse`=?, `datesStagesClasse`=? where `idClasse`=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $classe->getNom();
            $traitement->bindparam(1, $param1);
            $param2 = $classe->getSection();
            $traitement->bindparam(2, $param2);
            $param3 = $classe->getDetailSection();
            $traitement->bindparam(3, $param3);
            $param4 = $classe->getMissionsType();
            $traitement->bindparam(4, $param4);
            $param5 = $classe->getDatesStage();
            $traitement->bindparam(5, $param5);
            $param6 = $classe->getNum();
            $traitement->bindparam(6, $param6);
            $traitement->execute();
        }
        // sinon nouvelle commande : INSERT
        else {
            $query = "insert into `classe` (`nomClasse`, `sectionClasse`,`detailSectionClasse`, `missionsTypeClasse`, `datesStagesClasse`) values (?,?,?,?,?);";
            $traitement = $this->db->prepare($query);
            $param1 = $classe->getNom();
            $traitement->bindparam(1, $param1);
            $param2 = $classe->getSection();
            $traitement->bindparam(2, $param2);
            $param3 = $classe->getDetailSection();
            $traitement->bindparam(3, $param3);
            $param4 = $classe->getMissionsType();
            $traitement->bindparam(4, $param4);
            $param5 = $classe->getDatesStage();
            $traitement->bindparam(5, $param5);
            $traitement->execute();

            $classe->setNum($this->db->lastInsertId());
        }

        return $classe->getNum();
    }

    /**
     * Supprime la classe de la base.
     * 
     * Supprime de la base la classe passée en paramètre. 
     * 
     * @param classe Objet classe devant être supprimé.
     * @return boolean Retourne true si la suppression est un succès, false sinon.
     */
    public function delete(classe $classe) {
        $nbRows = 0;

        // la classe que nous essayons de supprimer existe-t-elle dans la  bd ?
        if ($classe->getNum() != '') {
            $query = "select count(*) as nb from `classe` where `idClasse`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $classe->getNum();
            $traitement->bindparam(1, $param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows = $ligne[0];
        }
        // Si la classe que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM classe
        //          et retourne TRUE
        if ($nbRows > 0) {
            // DELETE FROM classe
            $query = "DELETE FROM classe WHERE idClasse=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $classe->getNum();
            $traitement->bindparam(1, $param1);
            $traitement->execute();

            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }

    /**
     * Sélectionne une(des) classe(s) dans la base.
     * 
     * Méthode générique de SELECT qui renvoie un tableau de classes correspondant aux critères de sélection spécifiés.
     * Si aucun paramètre n'est précisé, la valeur par défaut du paramètre 'WHERE 1' permet d'obtenir toutes les commandes.
     * 
     * @param string Chaîne de caractère devant être une restriction SQL valide.
     * @return array Renvoie un tableau d'objet(s) classe.
     */
    public function getList($restriction = 'WHERE 1') {
        $query = "select * from `classe` " . $restriction . ";";
        $classeList = Array();

        //execution de la requete
        try {
            $result = $this->db->Query($query);
        } catch (PDOException $e) {
            die("Erreur : " . $e->getMessage());
        }

        //Parcours du jeu d'enregistrement
        //Chaque ligne comporte les colonnes numCde, dateCde, etatCde et idCliCde
        while ($row = $result->fetch()) {

            $classe = new classe($row['nomClasse'], $row['sectionClasse'], $row['detailSectionClasse'], $row['missionsTypeClasse'], $row['datesStagesClasse']);
            $classe->setNum($row['idClasse']);

            //ajout de l'objet commande bien rempli à la fin du tableau
            $classeList[] = $classe;
        }
        //retourne le tableau d'objets 'commande'
        return $classeList;
    }

    /**
     * Sélectionne une classe dans la base.
     * 
     * Méthode de SELECT qui renvoie la classe dont le numéro est spécifié en paramètre.
     * 
     * @param int Numéro de la classe recherchée
     * @return produit|boolean Renvoie la classe recherchée ou FALSE si elle n'a pas été trouvée
     */
    public function get($num) {
        $query = "select * from `classe` WHERE `idClasse`=?;";
        //Connection et execution de la requete
        try {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1, $num);
            $traitement->execute();
        } catch (PDOException $e) {
            die("Erreur : " . $e->getMessage());
        }
        //On récupère la première et seule ligne du jeu d'enregistrement	
        if ($row = $traitement->fetch()) {
            //appel du constructeur paramétré
            $classe = new classe($row['nomClasse'], $row['sectionClasse'], $row['detailSectionClasse'], $row['missionsTypeClasse'], $row['datesStagesClasse']);
            $classe->setNum($row['idClasse']);


            //retourne l'objet 'classe' correpsondant
            return $classe;
        } else {
            return false;
        }
    }

}
