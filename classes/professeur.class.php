<?php
/**
 * Classe représentant les professeurs.
 * 
 * Les professeurs sont définis par :
 *     <br>- un id unique
 *     <br>- un nom
 *     <br>- un prénom
 *     <br>- un mail
 *     <br>- une description
 *     <br>- un login
 *     <br>- un pass
 *     <br>- une classe
 *
 * @author Julien
 */
class professeur implements JsonSerializable {
    private $id="";
    private $nom="";
    private $prenom="";
    private $login="";
    private $pass="";
    private $lesClasses=array();
 
    /**
     * Instancie un objet professeur.
     *  
     * @param string Nom de professeur.
     * @param string Prénom de professeur.
     * @param string Mail de professeur.
     * @param string Description de professeur.
     * @param string Login de professeur.
     * @param string Pass de professeur.
     * @param string Classe de professeur.
     * 
     */
    public function __construct($nom, $pre, $login, $pass, $lesClasses){
        $this->nom = $nom;
        $this->prenom = $pre;
        $this->login = $login;
        $this->pass = $pass;
        $this->lesClasses = $lesClasses;
    }
    
    // Mutateurs chargés de modifier les attributs
    public function setId($id){$this->id = $id;}    
    public function setNom($n){$this->nom = $n;}
    public function setPrenom($p){$this->prenom = $p;}
    public function setLogin($l){$this->login = $l;}
    public function setPass($p){$this->pass = $p;}
    public function setLesClasses($c){$this->lesClasses = $c;}
    
    // Accesseurs chargés d'exposer les attributs
    public function getId(){return $this->id;}
    public function getNom(){return $this->nom;}
    public function getPrenom(){return $this->prenom;}
    public function getLogin(){return $this->login;}
    public function getPass(){return $this->pass;}
    public function getLesClasses(){return $this->lesClasses;}
    
    /**
     * Spécifie les données qui doivent être linéarisées en JSON.
     * 
     * Linéarise l'objet en une valeur qui peut être linéarisé nativement par la fonction json_encode().
     * 
     * @return mixed Retourne les données qui peuvent être linéarisées par la fonction json_encode()
     */
    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'nom' => $this->nom,
            'prenom' => $this->prenom,
            'classes' => $this->lesClasses
        ];
    }
}
