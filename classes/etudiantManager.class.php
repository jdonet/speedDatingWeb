<?php
require_once ("database.class.php");
require_once ("classeManager.class.php");

/**
 * Classe d'accès aux données concernant les etudiants.
 *
 * @author Julien
 */
class etudiantManager {
    
    private $db;
    
    /**
     * Instancie un objet etudiantManager.
     * 
     * Permet d'instanicer un objet etudiantManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    
    /**
     * Enregistre ou Modifie l'etudiant dans la base.
     * 
     * Pour enregistrer l'etudiant passé en paramètre en base de données :
     *      UPDATE si l'etudiant est déjà existant;
     *      INSERT sinon (si id non trouvé ou non spécifié).
     * 
     * @param produit Etudiant à enregister ou mettre à jour.
     * 
     * @return int Retourne l'id de l'etudiant ajouté ou mis à jour.
     */
    public function save(etudiant $etu)
    {        
        $nbRows = 0;

        // l'étudiant que nous essayons de sauvegarder existe-t-il dans la  base de données ?
        if ($etu->getId()!=''){
            $query = "select count(*) as nb from `etudiant` where `idEtudiant`=?";
            $traitement = $this->db->prepare($query);
            $param1=$etu->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }
        
        // Si l'étudiant que nous essayons de sauvegarder existe dans la base de données : UPDATE
        if ($nbRows > 0)
        {
            $query = "update `etudiant` set `nomEtudiant`=?, `prenomEtudiant`=?, `mailEtudiant`=?, `descriptionEtudiant`=?, `loginEtudiant`=?, `passEtudiant`=?, `classeEtudiant`=?, `telephoneEtudiant`=? where `idEtudiant`=?;";
            $traitement = $this->db->prepare($query);
            $param1=$etu->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$etu->getPrenom();
            $traitement->bindparam(2,$param2);
            $param3=$etu->getMail();
            $traitement->bindparam(3,$param3);
            $param4=$etu->getDescription();
            $traitement->bindparam(4,$param4);
            $param5=$etu->getLogin();
            $traitement->bindparam(5,$param5);
            $param6=$etu->getPass();
            $traitement->bindparam(6,$param6);
            $param7=$etu->getClasse()->getNum();
            $traitement->bindparam(7,$param7);
            $param8=$etu->getTelephone();
            $traitement->bindparam(8,$param8);
			$param9=$etu->getId();
			$traitement->bindparam(9,$param9);
            $traitement->execute();
            
        }
        // sinon : INSERT
        else
        {
            $query = "insert into `etudiant` (`nomEtudiant`, `prenomEtudiant`,`mailEtudiant`,`descriptionEtudiant`,`loginEtudiant`,`passEtudiant`,`classeEtudiant`,`telephoneEtudiant`) values (?,?,?,?,?,?,?,?);";
            $traitement = $this->db->prepare($query);
            $param1=$etu->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$etu->getPrenom();
            $traitement->bindparam(2,$param2);
            $param3=$etu->getMail();
            $traitement->bindparam(3,$param3);
            $param4=$etu->getDescription();
            $traitement->bindparam(4,$param4);
            $param5=$etu->getLogin();
            $traitement->bindparam(5,$param5);
            $param6=$etu->getPass();
            $traitement->bindparam(6,$param6);
            $param7=$etu->getClasse()->getNum();
            $traitement->bindparam(7,$param7);
            $param8=$etu->getTelephone();
            $traitement->bindparam(8,$param8);            
            $traitement->execute();
        }
        
        if ($etu->getId() == "")
        {
            $etu->setId($this->db->lastInsertId());
        }
        return $etu->getId();
    }

    /**
     * Supprime l'etudiant de la base.
     * 
     * Supprime de la base l'etudiant (table "etudiant").
     * 
     * @param etudiant Objet etudiant devant être supprimé.
     * @return boolean Retourne TRUE si la suppression est un succès, FALSE sinon.
     */    
    public function delete(etudiant $etu)
    {
        $nbRows = 0;

        // l'etudiant que nous essayons de supprimer existe-t-il dans la  base de données ?
        if ($etu->getId()!=''){                    
            $query = "select count(*) as nb from `etudiant` where `idEtudiant`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $etu->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // SI l'etudiant que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM etudiant
        //          et retourne TRUE
        if ($nbRows > 0)
        {            
            // DELETE FROM etudiant
            $query = "DELETE FROM etudiant WHERE idEtudiant=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $etu->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            
            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }

    /**
     * Sélectionne un(des) etudiant(s) dans la base.
     * 
     * Méthode générique de SELECT qui renvoie un tableau de etudiant correspondant aux critères de sélection spécifiés.
     * Si aucun paramètre n'est précisé, la valeur par défaut du paramètre 'WHERE 1' permet d'obtenir tous les etudiants.
     * 
     * @param string Chaîne de caractère devant être une restriction SQL valide.
     * @return array Renvoie un tableau d'objet(s) etudiant.
     */
    public function getList($restriction='WHERE 1')
    {
        $query = "select * from `etudiant` ".$restriction." ORDER BY nomEtudiant, prenomEtudiant;";
        $etuList = Array();

        //execution de la requete
        try
        {
            $result = $this->db->Query($query);
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //Parcours du jeu d'enregistrement
        while ($row = $result->fetch())
        {
            //récupération de la classe de l'étudiant
            $managerClasse = new classeManager(database::getDB());
            $classe = $managerClasse->get($row['classeEtudiant']);
            //On instancie un objet 'etudiant' avec les valeurs récupérées
            //appel du constructeur paramétré
            $etu = new etudiant($row['nomEtudiant'],$row['prenomEtudiant'],$row['mailEtudiant'],$row['descriptionEtudiant'],$row['loginEtudiant'],$row['passEtudiant'],$classe,$row['telephoneEtudiant']);
            //positionnement de l'id
            $etu->setId($row['idEtudiant']);
            //ajout de l'objet à la fin du tableau
            $etuList[] = $etu;
        }
        //retourne le tableau d'objets 'etudiant'
        return $etuList;  
    }
    
    /**
     * Sélectionne un etudiant dans la base.
     * 
     * Méthode de SELECT qui renvoie le etudiant dont l'id est spécifié en paramètre.
     * 
     * @param int ID du etudiant recherché
     * @return produit|boolean Renvoie l'objet etudiant recherché ou FALSE s'il n'a pas été trouvé
     */
    public function get($id)
    {
        $query = "select * from `etudiant` WHERE `idEtudiant`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            
            //récupération de la classe de l'étudiant
            $managerClasse = new classeManager(database::getDB());
            $classe = $managerClasse->get($row['classeEtudiant']);
                        //On instancie un objet 'etudiant' avec les valeurs récupérées
            //appel du constructeur paramétré
            $etu = new etudiant($row['nomEtudiant'],$row['prenomEtudiant'],$row['mailEtudiant'],$row['descriptionEtudiant'],$row['loginEtudiant'],$row['passEtudiant'],$classe,$row['telephoneEtudiant']);
            //positionnement de l'id
            $etu->setId($row['idEtudiant']);

            //retourne l'objet 'etudiant' correpsondant
            return $etu;
        }
        else {
            return false;
        }
    }
    
}
