<?php
require_once ("database.class.php");
require_once ("classeManager.class.php");

/**
 * Classe d'accès aux données concernant les profdiants.
 *
 * @author Julien
 */
class professeurManager {

    private $db;

    /**
     * Instancie un objet profdiantManager.
     *
     * Permet d'instanicer un objet profdiantManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }

    /**
     * Enregistre ou Modifie l'profdiant dans la base.
     *
     * Pour enregistrer l'profdiant passé en paramètre en base de données :
     *      UPDATE si l'profdiant est déjà existant;
     *      INSERT sinon (si id non trouvé ou non spécifié).
     *
     * @param produit profdiant à enregister ou mettre à jour.
     *
     * @rprofrn int Retourne l'id de l'profdiant ajouté ou mis à jour.
     */
    public function save(professeur $prof)
    {
        $nbRows = 0;

        // l'étudiant que nous essayons de sauvegarder existe-t-il dans la  base de données ?
        if ($prof->getId()!=''){
            $query = "select count(*) as nb from `professeur` where `idProf`=?";
            $traitement = $this->db->prepare($query);
            $param1=$prof->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // Si l'étudiant que nous essayons de sauvegarder existe dans la base de données : UPDATE
        if ($nbRows > 0)
        {
            $query = "update `professeur` set `nomProf`=?, `prenomProf`=?, `mailProf`=?, `descriptionProf`=?, `loginProf`=?, `passProf`=?, `classeProf`=? where `idProf`=?;";
            $traitement = $this->db->prepare($query);
            $param1=$prof->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$prof->getPrenom();
            $traitement->bindparam(2,$param2);
            $param3=$prof->getLogin();
            $traitement->bindparam(3,$param3);
            $param4=$prof->getPass();
            $traitement->bindparam(4,$param4);
            $param5=$prof->getClasses()->getId();
            $traitement->bindparam(5,$param5);
            $param6=$prof->getId();
            $traitement->bindparam(6,$param6);
            $traitement->execute();
        }
        // sinon : INSERT
        else
        {
            $query = "insert into `professeur` (`nomProf`, `prenomProf`,`loginProf`,`passProf`,`classesProf`) values (?,?,?,?,?);";
            $traitement = $this->db->prepare($query);
            $param1=$prof->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$prof->getPrenom();
            $traitement->bindparam(2,$param2);
            $param3=$prof->getLogin();
            $traitement->bindparam(3,$param3);
            $param4=$prof->getPass();
            $traitement->bindparam(4,$param4);
            $param5=$prof->getClasses()->getId();
            $traitement->bindparam(5,$param5);
            $traitement->execute();
        }

        if ($prof->getId() == "")
        {
            $prof->setId($this->db->lastInsertId());
        }
        return $prof->getId();
    }

    /**
     * Supprime l'profdiant de la base.
     *
     * Supprime de la base l'profdiant (table "profdiant").
     *
     * @param profdiant Objet profdiant devant être supprimé.
     * @rprofrn boolean Retourne TRUE si la suppression est un succès, FALSE sinon.
     */
    public function delete(professeur $prof)
    {
        $nbRows = 0;

        // l'profdiant que nous essayons de supprimer existe-t-il dans la  base de données ?
        if ($prof->getId()!=''){
            $query = "select count(*) as nb from `professeur` where `idProf`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $prof->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // SI l'profdiant que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM profdiant
        //          et retourne TRUE
        if ($nbRows > 0)
        {
            // DELETE FROM profdiant
            $query = "DELETE FROM professeur WHERE idProf=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $prof->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();

            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }

    /**
     * Sélectionne un(des) profdiant(s) dans la base.
     *
     * Méthode générique de SELECT qui renvoie un tableau de profdiant correspondant aux critères de sélection spécifiés.
     * Si aucun paramètre n'est précisé, la valeur par défaut du paramètre 'WHERE 1' permet d'obtenir tous les profdiants.
     *
     * @param string Chaîne de caractère devant être une restriction SQL valide.
     * @rprofrn array Renvoie un tableau d'objet(s) profdiant.
     */
    public function getList($restriction='WHERE 1')
    {
        $query = "select * from `professeur` ".$restriction.";";
        $profList = Array();

        //execution de la requete
        try
        {
            $result = $this->db->Query($query);
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //Parcours du jeu d'enregistrement
        while ($row = $result->fetch())
        {
            //récupération des classes du prof
            $lesClasses = $this->getClassesProf($row['idProf']);

            //On instancie un objet 'profdiant' avec les valeurs récupérées
            //appel du constructeur paramétré
            $prof = new professeur($row['nomProf'],$row['prenomProf'],$row['loginProf'],$row['passProf'],$lesClasses);
            //positionnement de l'id
            $prof->setId($row['idProf']);
            //ajout de l'objet à la fin du tableau
            $profList[] = $prof;
        }
        //retourne le tableau d'objets 'profdiant'
        return $profList;
    }

    /**
     * Sélectionne un profdiant dans la base.
     *
     * Méthode de SELECT qui renvoie le profdiant dont l'id est spécifié en paramètre.
     *
     * @param int ID du profdiant recherché
     * @rprofrn produit|boolean Renvoie l'objet profdiant recherché ou FALSE s'il n'a pas été trouvé
     */
    public function get($id)
    {
        $query = "select * from `professeur` WHERE `idProf`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement
        if($row = $traitement->fetch()) {

            //récupération des classes du prof
            $lesClasses = $this->getClassesProf($id);

            //On instancie un objet 'profdiant' avec les valeurs récupérées
            //appel du constructeur paramétré
            $prof = new professeur($row['nomProf'],$row['prenomProf'],$row['loginProf'],$row['passProf'],$lesClasses);
            //positionnement de l'id
            $prof->setId($row['idProf']);

            //retourne l'objet 'profdiant' correpsondant
            return $prof;
        }
        else {
            return false;
        }
    }

    private function getClassesProf($id)
    {
        $query = "select * from `enseigner` WHERE `idProf`=?;";
        $classesList = Array();

        //execution de la requete
        try
        {
          $traitement = $this->db->prepare($query);
          $traitement->bindparam(1,$id);
          $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        $managerClasse = new classeManager(database::getDB());
        //Parcours du jeu d'enregistrement
        while ($row = $traitement->fetch())
        {
            $classe = $managerClasse->get($row['idClasse']);
            //ajout de l'objet à la fin du tableau
            $classesList[] = $classe;
        }
        return $classesList;
    }


}
