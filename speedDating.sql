-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 21, 2018 at 10:30 AM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `speedDating`
--
CREATE DATABASE IF NOT EXISTS `speedDating` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `speedDating`;

-- --------------------------------------------------------

--
-- Table structure for table `classe`
--

CREATE TABLE `classe` (
  `idClasse` int(11) NOT NULL,
  `nomClasse` varchar(20) NOT NULL,
  `sectionClasse` varchar(20) NOT NULL,
  `detailSectionClasse` varchar(200) NOT NULL,
  `datesStagesClasse` varchar(255) NOT NULL,
  `missionsTypeClasse` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classe`
--

INSERT INTO `classe` (`idClasse`, `nomClasse`, `sectionClasse`, `detailSectionClasse`, `datesStagesClasse`, `missionsTypeClasse`) VALUES
(1, '1SIO', 'SIO', 'Première année de Services Informatiques aux Organisations', 'mai/juin', 'sites statiques'),
(2, '2SIO', 'SIO', 'Seconde année de Services Informatiques aux Organisations', 'janvier/février', 'sites dynamiques, applications mobiles');

-- --------------------------------------------------------

--
-- Table structure for table `contenuListe`
--

CREATE TABLE `contenuListe` (
  `refListe` int(11) NOT NULL,
  `refEtudiant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contenuListe`
--

INSERT INTO `contenuListe` (`refListe`, `refEtudiant`) VALUES
(1, 1),
(1, 2),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `enseigner`
--

CREATE TABLE `enseigner` (
  `idProf` int(11) NOT NULL,
  `idClasse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enseigner`
--

INSERT INTO `enseigner` (`idProf`, `idClasse`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `etudiant`
--

CREATE TABLE `etudiant` (
  `idEtudiant` int(11) NOT NULL,
  `nomEtudiant` varchar(200) CHARACTER SET latin1 NOT NULL,
  `prenomEtudiant` varchar(200) CHARACTER SET latin1 NOT NULL,
  `mailEtudiant` varchar(200) CHARACTER SET latin1 NOT NULL,
  `telephoneEtudiant` varchar(255) DEFAULT NULL,
  `descriptionEtudiant` varchar(250) NOT NULL,
  `loginEtudiant` varchar(100) CHARACTER SET latin1 NOT NULL,
  `passEtudiant` varchar(200) CHARACTER SET latin1 NOT NULL,
  `classeEtudiant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `etudiant`
--

INSERT INTO `etudiant` (`idEtudiant`, `nomEtudiant`, `prenomEtudiant`, `mailEtudiant`, `telephoneEtudiant`, `descriptionEtudiant`, `loginEtudiant`, `passEtudiant`, `classeEtudiant`) VALUES
(1, 'Agnie', 'Narii', 'agnie.narii@gmail.com', '+68987765135', 'je suis un mec xD j\'aime tout', 'test', 'test', 2),
(2, 'Chan-Tagi', 'Bomeck', '', '', '', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `liste`
--

CREATE TABLE `liste` (
  `idListe` int(11) NOT NULL,
  `nomOrganisationListe` varchar(200) NOT NULL,
  `statutListe` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `liste`
--

INSERT INTO `liste` (`idListe`, `nomOrganisationListe`, `statutListe`) VALUES
(1, 'Imakumo', 1),
(2, 'Vittoria Conseil', 1);

-- --------------------------------------------------------

--
-- Table structure for table `professeur`
--

CREATE TABLE `professeur` (
  `idProf` int(255) NOT NULL,
  `nomProf` varchar(30) CHARACTER SET latin1 NOT NULL,
  `prenomProf` varchar(30) CHARACTER SET latin1 NOT NULL,
  `mailProf` varchar(100) NOT NULL,
  `descriptionProf` varchar(200) NOT NULL,
  `loginProf` varchar(200) NOT NULL,
  `passProf` varchar(200) NOT NULL,
  `classeProf` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `professeur`
--

INSERT INTO `professeur` (`idProf`, `nomProf`, `prenomProf`, `mailProf`, `descriptionProf`, `loginProf`, `passProf`, `classeProf`) VALUES
(1, 'Donet', 'Julien', '', '', 'root', 'root', 0),
(2, 'Missonier', 'Fabrice', '', '', '0', '0', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classe`
--
ALTER TABLE `classe`
  ADD PRIMARY KEY (`idClasse`);

--
-- Indexes for table `contenuListe`
--
ALTER TABLE `contenuListe`
  ADD PRIMARY KEY (`refListe`,`refEtudiant`);

--
-- Indexes for table `enseigner`
--
ALTER TABLE `enseigner`
  ADD PRIMARY KEY (`idProf`,`idClasse`),
  ADD KEY `idClasse` (`idClasse`);

--
-- Indexes for table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`idEtudiant`),
  ADD KEY `classeEtudiant` (`classeEtudiant`);

--
-- Indexes for table `liste`
--
ALTER TABLE `liste`
  ADD PRIMARY KEY (`idListe`);

--
-- Indexes for table `professeur`
--
ALTER TABLE `professeur`
  ADD PRIMARY KEY (`idProf`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classe`
--
ALTER TABLE `classe`
  MODIFY `idClasse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `idEtudiant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `liste`
--
ALTER TABLE `liste`
  MODIFY `idListe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `professeur`
--
ALTER TABLE `professeur`
  MODIFY `idProf` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `contenuListe`
--
ALTER TABLE `contenuListe`
  ADD CONSTRAINT `contenuListe_ibfk_1` FOREIGN KEY (`refListe`) REFERENCES `liste` (`idListe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `enseigner`
--
ALTER TABLE `enseigner`
  ADD CONSTRAINT `enseigner_ibfk_1` FOREIGN KEY (`idProf`) REFERENCES `professeur` (`idProf`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `enseigner_ibfk_2` FOREIGN KEY (`idClasse`) REFERENCES `classe` (`idClasse`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `etudiant`
--
ALTER TABLE `etudiant`
  ADD CONSTRAINT `etudiant_ibfk_1` FOREIGN KEY (`classeEtudiant`) REFERENCES `classe` (`idClasse`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;