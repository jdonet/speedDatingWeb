<?php

require_once ("../classes/database.class.php");

require_once ("../classes/classe.class.php");
require_once ("../classes/classeManager.class.php");
require_once ("../classes/etudiant.class.php");
require_once ("../classes/etudiantManager.class.php");
require_once ("../classes/liste.class.php");
require_once ("../classes/listeManager.class.php");

// Retourne la liste de toutes les classes
function getClasses() {
    $manager = new classeManager(database::getDB());
    $tabClasses = $manager->getList();
    echo json_encode($tabClasses);
}

// Retourne la classe dont l'id est passé en paramètre
function getClasseId($idClasse) {
    $manager = new classeManager(database::getDB());
    $tabClasses = $manager->get($idClasse);
    echo json_encode($tabClasses);
}
// Retourne la liste de tous les étudiants
function getEtudiants() {
    $manager = new etudiantManager(database::getDB());
    $tabEtudiants = $manager->getList();
    echo json_encode($tabEtudiants);
}

// Retourne l'étudiant dont l'id est passé en paramètre
function getEtudiantId($idEtudiant) {
    $manager = new etudiantManager(database::getDB());
    $tabEtudiants = $manager->get($idEtudiant);
    echo json_encode($tabEtudiants);
}

// Retourne toutes les listes
function getListes() {
    $manager = new listeManager(database::getDB());
    $tabListes = $manager->getList();
    echo json_encode($tabListes);
}

// Retourne toutes les listes
function getListesAImprimer() {
    $manager = new listeManager(database::getDB());
    $tabListes = $manager->getList("WHERE statutListe=0");
    echo json_encode($tabListes);
}

// Retourne la liste dont l'id est passé en paramètre
function getListeId($idListe) {
    $manager = new listeManager(database::getDB());
    $tabListes = $manager->get($idListe);
    echo json_encode($tabListes);
}

// Insère une nouvelle liste à imprimer
function imprimerListe($organisation, $etudiants) {
    $managerListe = new listeManager(database::getDB());
    $managerEtudiant = new etudiantManager(database::getDB());
    
    $liste = new liste($organisation);
    $idEtudiants = explode(";",$etudiants);
    
    for ($index = 0; $index < count($idEtudiants); $index++) {//pour chaque étudiant, s'il existe dans la bdd on l'ajoute à la liste
        $etudiant = $managerEtudiant->get($idEtudiants[$index]);
        if($etudiant){
            $liste->addEtudiant($etudiant);
        }
    }
    
    $managerListe->save($liste); //enregistrement de la liste en bdd
    echo json_encode($liste);
}