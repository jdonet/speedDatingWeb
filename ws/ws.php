<?php

require_once("facade.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $PARRAY = $_POST;
} else {
    $PARRAY = $_GET;
}

/* selection de l'action */
if (isset($PARRAY["action"])) {
    $act = $PARRAY["action"];

    if ($act == "getClasses") {
        getClasses();
    } else if ($act == "getClasseId") {
        getClasseId($PARRAY["id"]);
    } else if ($act == "getEtudiants") {
        getEtudiants();
    } else if ($act == "getEtudiantId") {
        getEtudiantId($PARRAY["id"]);
    } else if ($act == "getListes") {
        getListes();
    } else if ($act == "getListeId") {
        getListeId($PARRAY["id"]);
    } else if ($act == "getListesAImprimer") {
        getListesAImprimer();
    } else if ($act == "imprimerListe") {
        imprimerListe($PARRAY["organisation"],$PARRAY["etudiants"]);
    } else {
        echo("Action inconnue !");
    }
} else {
    echo ("Erreur, vérifiez les paramètres de votre requête http !");
}


